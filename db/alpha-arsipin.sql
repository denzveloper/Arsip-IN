-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 30 Bulan Mei 2018 pada 23.18
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alpha-arsipin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_data`
--

CREATE TABLE `tbl_data` (
  `username` varchar(24) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dibaca` bit(1) NOT NULL,
  `date` datetime NOT NULL,
  `year` year(4) NOT NULL,
  `k_no_srt_pk_kd_kls` bit(1) NOT NULL,
  `k_pk_nk_ag` bit(1) NOT NULL,
  `k_sim_ar_fol` bit(1) NOT NULL,
  `k_sim_ar_bin` bit(1) NOT NULL,
  `k_sim_ar_box` bit(1) NOT NULL,
  `k_sim_ar_cab` bit(1) NOT NULL,
  `k_tata_arsip` int(1) NOT NULL,
  `m_pk_agenda` bit(1) NOT NULL,
  `m_pk_lmbr_dispo` bit(1) NOT NULL,
  `m_sim_ar_fol` bit(1) NOT NULL,
  `m_sim_ar_bin` bit(1) NOT NULL,
  `m_sim_ar_box` bit(1) NOT NULL,
  `m_sim_ar_cab` bit(1) NOT NULL,
  `m_tata_arsip` int(1) NOT NULL,
  `sk_olah_arsip` bit(1) NOT NULL,
  `arsip_thn_sblm` bit(1) NOT NULL,
  `tahun` year(4) DEFAULT NULL,
  `sim_ar_fol` bit(1) NOT NULL,
  `sim_ar_box` bit(1) NOT NULL,
  `sim_ar_gdng` bit(1) NOT NULL,
  `sim_ar_lemari` bit(1) NOT NULL,
  `hsl_temu` varchar(2048) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ar_tanah` bit(1) NOT NULL,
  `ar_apbdes` bit(1) NOT NULL,
  `ar_kuang` bit(1) NOT NULL,
  `ar_kpdn` bit(1) NOT NULL,
  `fto_mntn_kuwu` bit(1) NOT NULL,
  `sjrh_desa` bit(1) NOT NULL,
  `ar_pilwu` bit(1) NOT NULL,
  `pta_desa` bit(1) NOT NULL,
  `ar_kep_skpmng` bit(1) NOT NULL,
  `ar_kep_ijazah` bit(1) NOT NULL,
  `ar_kep_akta` bit(1) NOT NULL,
  `ar_kep_kk` bit(1) NOT NULL,
  `ar_kep_bknkh` bit(1) NOT NULL,
  `ar_kep_ktp` bit(1) NOT NULL,
  `mpg_sri` bit(1) NOT NULL,
  `sdkh_bumi` bit(1) NOT NULL,
  `ngnjng_bar` bit(1) NOT NULL,
  `seni_daerah` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `s_box_ar` bit(1) NOT NULL,
  `s_fol_map` bit(1) NOT NULL,
  `s_skat` bit(1) NOT NULL,
  `s_label` bit(1) NOT NULL,
  `s_fill_cab` bit(1) NOT NULL,
  `sdm_krg_mmdai` bit(1) NOT NULL,
  `blm_phm_atrn` bit(1) NOT NULL,
  `bts_sarana` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_data`
--

INSERT INTO `tbl_data` (`username`, `dibaca`, `date`, `year`, `k_no_srt_pk_kd_kls`, `k_pk_nk_ag`, `k_sim_ar_fol`, `k_sim_ar_bin`, `k_sim_ar_box`, `k_sim_ar_cab`, `k_tata_arsip`, `m_pk_agenda`, `m_pk_lmbr_dispo`, `m_sim_ar_fol`, `m_sim_ar_bin`, `m_sim_ar_box`, `m_sim_ar_cab`, `m_tata_arsip`, `sk_olah_arsip`, `arsip_thn_sblm`, `tahun`, `sim_ar_fol`, `sim_ar_box`, `sim_ar_gdng`, `sim_ar_lemari`, `hsl_temu`, `ar_tanah`, `ar_apbdes`, `ar_kuang`, `ar_kpdn`, `fto_mntn_kuwu`, `sjrh_desa`, `ar_pilwu`, `pta_desa`, `ar_kep_skpmng`, `ar_kep_ijazah`, `ar_kep_akta`, `ar_kep_kk`, `ar_kep_bknkh`, `ar_kep_ktp`, `mpg_sri`, `sdkh_bumi`, `ngnjng_bar`, `seni_daerah`, `s_box_ar`, `s_fol_map`, `s_skat`, `s_label`, `s_fill_cab`, `sdm_krg_mmdai`, `blm_phm_atrn`, `bts_sarana`) VALUES
('akua', b'1', '2018-05-30 15:15:57', 2018, b'1', b'1', b'1', b'1', b'1', b'1', 1, b'1', b'1', b'1', b'1', b'1', b'1', 1, b'1', b'1', 0000, b'1', b'1', b'1', b'1', 'KOKOKO', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1', 'kokokoko', b'1', b'1', b'1', b'1', b'1', b'1', b'1', b'1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kode`
--

CREATE TABLE `tbl_kode` (
  `kodever` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_kode`
--

INSERT INTO `tbl_kode` (`kodever`) VALUES
('PG6E57C'),
('O9SNKLV'),
('HFABM6I'),
('RCSERMV'),
('JRDFIGW'),
('VCY9PGP'),
('A1ZLB05'),
('QIO8HBY'),
('QB3AUZS'),
('NJH1PXL'),
('FNIJOZG'),
('TXEVUBH'),
('LDU3KOA'),
('HOVL5DR'),
('LEIKWOH'),
('0UE9VAS'),
('G60BVT2'),
('E28YCYO'),
('9H7C4VX'),
('KIP3QFM'),
('CZ9LJXW'),
('TBH4GQE'),
('NICJOQU'),
('W7F9EVX'),
('LPTYKX9'),
('GOYRAJS'),
('KXTR7MB'),
('JQG60NB'),
('SPM9XMQ'),
('L8YUZ0D'),
('TGMCY7R'),
('G3JKEAX');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `username` varchar(24) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nama_user` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `jabatan` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `place` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` varchar(18) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `level` bit(1) NOT NULL,
  `acccreate` datetime NOT NULL,
  `lastlog` datetime NOT NULL,
  `lastedit` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`username`, `nama_user`, `jabatan`, `place`, `password`, `phone`, `level`, `acccreate`, `lastlog`, `lastedit`) VALUES
('admin', 'Administrator', '', '', '|fbp', '12345598', b'0', '2018-03-29 18:33:34', '2018-05-30 21:27:50', '2018-04-30 14:31:54'),
('aku', 'Aku', 'Perintis', 'LOHSALAH', 't`t', '009988877', b'1', '2018-04-30 19:37:23', '2018-05-30 13:16:39', '2018-04-30 19:37:23'),
('akua', 'Akuah Tututututu', 'Manager', 'DAH NONE', '`~~`', '88881', b'1', '2018-05-02 13:08:58', '2018-05-30 14:34:32', '2018-05-02 13:08:58');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_data`
--
ALTER TABLE `tbl_data`
  ADD KEY `indexdata` (`username`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `place` (`place`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_data`
--
ALTER TABLE `tbl_data`
  ADD CONSTRAINT `tbl_data_ibfk_1` FOREIGN KEY (`username`) REFERENCES `tbl_users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
