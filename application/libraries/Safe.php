<?php
//Safe veri 0.2
defined('BASEPATH') OR exit('No direct script access allowed');

class Safe extends CI_Model{
	//anti inject
	function inject($string){
		$result = addslashes(trim($string));
		return $result;
	}

	function auto($x){
		if($x==1){
			return "YA";
		}else{
			return "TIDAK";
		}
	}

	function autp($x){
		if($x==1){
			return "SUDAH";
		}else{
			return "BELUM";
		}
	}

	function autoa($x){
		if($x==1){
			return "ADA";
		}else{
			return "TIDAK ADA";
		}
	}

	function autox($x){
		if($x==1){
			return "Berdasarkan Klasifikasi";
		}elseif($x==2){
			return "Berdasarkan Urutan Waktu";
		}elseif($x==0){
			return "Ditumpuk Tidak Beraturan";
		}else{
			return "*INVALID*";
		}
	}

	//enkripsi data
	function convert($string,$key){
		$str=strval($string);
		$ky=strrev($key);
		$ky=str_replace(chr(32),'',$ky);
		$kl=strlen($ky)<32?strlen($ky):32;
		$k=array();for($i=0;$i<$kl;$i++){
		$k[$i]=ord($ky{$i})&0x1F;}
		$j=0;for($i=0;$i<strlen($str);$i++){
		$e=ord($str{$i});
		$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e);
		$j++;$j=$j==$kl?0:$j;}
		return addslashes(trim($str));
	}
}