<?php
//Dataio(Data I/O) versi 0.3
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataio extends CI_Model{

    //Get for edit data
    function gtedt($field){
        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->where($field);
        $this->db->order_by("date","DESC");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    //Pengecekan apa ada user: $field
    function chk_us($field){
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where($field);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return TRUE;
        }
    }

    //Pengecekan apa ada data terbaru dari $field
    function chk_ok($field){
        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->where($field);
        $this->db->order_by("date","DESC");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return TRUE;
        }
    }

    //Inserting data
    function datain($tabel, $data){
        $this->db->insert($tabel, $data);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //fungsi untuk Update data
    function dataup($field, $who){
        $this->db->where($who);
        $this->db->update("tbl_data", $field);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Pengecekan
    function chk($tabel, $field){
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->where($field);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return TRUE;
        }
    }

	//Fetching data All
    function viewall(){
        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->order_by("date", "DESC");
        $this->db->limit(10);
        $query = $this->db->get();
	    $data = array();
	    if($query !== FALSE && $query->num_rows() > 0){
	    	foreach ($query->result() as $row) {
	    		$data[] = $row;
	    	}
	    }
		return $data;
    }

	//Lihat data detail
	function viewdata($field){
        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->where($field);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Buat data hanya bisa baca
    function makejr($who){
    	$this->db->where($who);
    	$this->db->where('dibaca',0);
        $this->db->update('tbl_data', array('dibaca' => 1));
    }

    //Fetching data Filtered
    function viewmin($field){
        $this->db->select('*');
        $this->db->from('tbl_data');
        $this->db->where($field);
        $query = $this->db->get();
	    $data = array();
	    if($query !== FALSE && $query->num_rows() > 0){
	    	foreach ($query->result() as $row) {
	    		$data[] = $row;
	    	}
	    }
	   	return $data;
    }

    //Chart use only
    function vica(){
        $y=date('Y');
        $this->db->from('tbl_data');
        $this->db->where('year',"$y");
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Dapatkan daftar user beserta dan disusun tempat
    function getlistuser(){
        $this->db->from('tbl_users');
        $this->db->where('level',1);
        $this->db->order_by("place");
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Dapatkan daftar laporan terbaru
    function getnewdata(){
        $this->db->from('tbl_data');
        $this->db->order_by("dibaca");
        $this->db->order_by("date","DESC");
        $this->db->limit(5);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Dapatkan list
    function getwho($find){
        $this->db->from('tbl_users');
        $this->db->where("username",$find);
        $query = $this->db->get();
        return $query->row();
    }

    //Delete Data
    public function del_data($field){
       $this->db->where($field);
       $this->db->delete("tbl_data");
       $query = $this->db->affected_rows();
       if ($query == 0) {
           return FALSE;
       }else{
           return $query;
       }
    }

}
