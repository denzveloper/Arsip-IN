			    <h3 class="panel-title"><i class="fas fa-file-alt"></i> Lihat Data Laporan <?php if($this->dataio->chk_us(array('username' => $this->safe->convert($this->input->get("usr", TRUE),$this->session->userdata('namaus'))))){ echo "\"".$this->dataio->getwho($this->safe->convert($this->input->get("usr", TRUE),$this->session->userdata('namaus')))->nama_user."\"";}else{echo "*User: '".$this->safe->convert($this->input->get("usr", TRUE),$this->session->userdata('namaus'))."' is 'INVALID'*";}?></h3>
			  </div>
			  <div class="panel-body">
			    <?php if($doc != FALSE){foreach($doc as $fill){ ?>
			    <h4>Laporan Tahun: <?php echo $fill->year." (".$this->dataio->getwho($fill->username)->place.")" ?></h4>
				<table>
					<tr>
						<td>Nama Pengisi/Jabatan</td><td>: <?php echo $this->dataio->getwho($fill->username)->nama_user." (<i>".$this->dataio->getwho($fill->username)->jabatan."</i>)"; ?></td>
					</tr>
					<tr>
						<td>Kontak</td><td>: <?php echo $this->dataio->getwho($fill->username)->phone; ?></td>
					</tr>
					<tr>
						<td>Hari, Tanggal</td><td>: <?php echo strftime("%A, %d %B %Y", strtotime($fill->date)); ?></td>
					</tr>
					<tr>
						<td>Pukul</td><td>: <?php echo strftime("%H:%M:%S", strtotime($fill->date)); ?></td>
					</tr>
					<tr>
						<td>Laporan Tahun</td><td>: <?php echo $fill->year; ?></td>
					</tr>
					<tr>
						<td colspan="2">
							<ol>
								<li>ARSIP AKTIF</li>
								<table class="table table-bordered">
									<tr>
										<th colspan="2">Pengurusan Surat Keluar</th>
									</tr>
									<tr>
										<td>Penomoran menggunakan kode klarifikasi</td><td><?php echo $this->safe->auto($fill->k_no_srt_pk_kd_kls); ?></td>
									</tr>
									<tr>
										<td>Memakai buku agenda</td><td><?php echo $this->safe->auto($fill->k_pk_nk_ag); ?></td>
									</tr>
									<tr>
										<td>Penyimpanan Arsip</td>
										<td>
											<table>
												<tr>
													<td>Folder</td><td>: <?php echo $this->safe->auto($fill->k_sim_ar_fol); ?></td>
												</tr>
												<tr>
													<td>Binder</td><td>: <?php echo $this->safe->auto($fill->k_sim_ar_bin); ?></td>
												</tr>
												<tr>
													<td>Box Arip</td><td>: <?php echo $this->safe->auto($fill->k_sim_ar_box); ?></td>
												</tr>
												<tr>
													<td>Filling Cabinet</td><td>: <?php echo $this->safe->auto($fill->k_sim_ar_cab); ?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>Penataan Arsip</td><td><?php echo $this->safe->autox($fill->k_tata_arsip); ?></td>
									</tr>
									<tr>
										<th colspan="2">Pengurusan Surat Masuk</th>
									</tr>
									<tr>
										<td>Memakai buku agenda</td><td><?php echo $this->safe->auto($fill->m_pk_agenda); ?></td>
									</tr>
									<tr>
										<td>Memakai lembar disposisi</td><td><?php echo $this->safe->auto($fill->m_pk_lmbr_dispo); ?></td>
									</tr>
									<tr>
										<td>Penyimpanan Arsip</td>
										<td>
											<table>
												<tr>
													<td>Folder</td><td>: <?php echo $this->safe->auto($fill->m_sim_ar_fol); ?></td>
												</tr>
												<tr>
													<td>Binder</td><td>: <?php echo $this->safe->auto($fill->m_sim_ar_bin); ?></td>
												</tr>
												<tr>
													<td>Box Arip</td><td>: <?php echo $this->safe->auto($fill->m_sim_ar_box); ?></td>
												</tr>
												<tr>
													<td>Filling Cabinet</td><td>: <?php echo $this->safe->auto($fill->m_sim_ar_cab); ?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>Penataan Arsip</td><td><?php echo $this->safe->auto($fill->m_tata_arsip); ?></td>
									</tr>
									<tr>
										<th>Pengurusan Surat Masuk</th><td><?php echo $this->safe->auto($fill->sk_olah_arsip); ?></td>
									</tr>
								</table>
								<li>ARSIP IN-AKTIF</li>
								<table class="table table-bordered">
									<tr>
										<td>Arsip Tahun Sebelumnya</td><td><?php echo $this->safe->autoa($fill->arsip_thn_sblm); if($fill->arsip_thn_sblm==1){ echo "<br>(Tahun: $fill->tahun)"; }?></td>
									</tr>
									<tr>
										<td>Penyimpanan Arsip</td>
										<td>
											<table>
												<tr>
													<td>Folder</td><td>: <?php echo $this->safe->auto($fill->sim_ar_fol); ?></td>
												</tr>
												<tr>
													<td>Binder</td><td>: <?php echo $this->safe->auto($fill->sim_ar_box); ?></td>
												</tr>
												<tr>
													<td>Box Arip</td><td>: <?php echo $this->safe->auto($fill->sim_ar_gdng); ?></td>
												</tr>
												<tr>
													<td>Filling Cabinet</td><td>: <?php echo $this->safe->auto($fill->sim_ar_lemari); ?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<li>ARSIP STATIS(DESA)</li>
								<table class="table table-bordered">
									<tr>
										<td>Arsip Tanah/Letter C</td><td><?php echo $this->safe->autoa($fill->ar_tanah);?></td>
									</tr>
									<tr>
										<td>Arsip APBDes</td><td><?php echo $this->safe->autoa($fill->ar_apbdes);?></td>
									</tr>
									<tr>
										<td>Arsip Keuangan</td><td><?php echo $this->safe->autoa($fill->ar_kuang);?></td>
									</tr>
									<tr>
										<td>Arsip Kependudukan</td><td><?php echo $this->safe->autoa($fill->ar_kpdn);?></td>
									</tr>
									<tr>
										<td>Foto Mantan Kuwu</td><td><?php echo $this->safe->autoa($fill->fto_mntn_kuwu);?></td>
									</tr>
									<tr>
										<td>Sejarah Desa Desa</td><td><?php echo $this->safe->autoa($fill->sjrh_desa);?></td>
									</tr>
									<tr>
										<td>Arsip PILWU</td><td><?php echo $this->safe->autoa($fill->ar_pilwu);?></td>
									</tr>
									<tr>
										<td>Peta Desa</td><td><?php echo $this->safe->autoa($fill->pta_desa);?></td>
									</tr>
									<tr>
										<td>Arsip Kepegawaian</td>
										<td>
											<table>
												<tr>
													<td>SK Pamong</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_skpmng);?></td>
												</tr>
												<tr>
													<td>Ijasah</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_ijazah);?></td>
												</tr>
												<tr>
													<td>Akte Kelahiran</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_akta);?></td>
												</tr>
												<tr>
													<td>Kartu Keluarga</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_kk);?></td>
												</tr>
												<tr>
													<td>Buku Nikah</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_bknkh);?></td>
												</tr>
												<tr>
													<td>KTP</td><td>: <?php echo $this->safe->autoa($fill->ar_kep_ktp);?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<li>ADAT DESA/ADAT ISTIADAT</li>
								<table class="table table-bordered">
									<tr>
										<td>Mapag Sri</td><td><?php echo $this->safe->autoa($fill->mpg_sri);?></td>
									</tr>
									<tr>
										<td>Sedekah Bumi</td><td><?php echo $this->safe->autoa($fill->sdkh_bumi);?></td>
									</tr>
									<tr>
										<td>Ngunjung/Baritan</td><td><?php echo $this->safe->autoa($fill->ngnjng_bar);?></td>
									</tr>
									<tr>
										<td>Kesenian Daerah</td><td><?php echo $fill->seni_daerah;?></td>
									</tr>
								</table>
								<li>SARANA KEARSIPAN</li>
								<table class="table table-bordered">
									<tr>
										<td>Box Arsip</td><td><?php echo $this->safe->autoa($fill->s_box_ar);?></td>
									</tr>
									<tr>
										<td>Folder/Map</td><td><?php echo $this->safe->autoa($fill->s_fol_map);?></td>
									</tr>
									<tr>
										<td>Skat</td><td><?php echo $this->safe->autoa($fill->s_skat);?></td>
									</tr>
									<tr>
										<td>Label</td><td><?php echo $this->safe->autoa($fill->s_label);?></td>
									</tr>
									<tr>
										<td>Filling Cabinet</td><td><?php echo $this->safe->autoa($fill->s_fill_cab);?></td>
									</tr>
								</table>
								<li>KESIMPULAN</li>
								<table class="table table-bordered">
									<tr>
										<td>SDM Kurang</td><td><?php echo $this->safe->auto($fill->sdm_krg_mmdai);?></td>
									</tr>
									<tr>
										<td>Belum Memahami Aturan</td><td><?php echo $this->safe->auto($fill->blm_phm_atrn);?></td>
									</tr>
									<tr>
										<td>Keterbatasan</td><td><?php echo $this->safe->auto($fill->bts_sarana);?></td>
									</tr>
								</table>
								<li>HASIL TEMUAN</li>
								<table class="table table-bordered">
								<tr>
									<td><?php echo $fill->hsl_temu; ?></td>
								</tr>
							</ol>
						</td>
					</tr>
				</table>
				<a href="<?php echo base_url('index.php/data/print').'?usr='.$this->input->get("usr", TRUE).'&dat='.$this->input->get("dat", TRUE); ?>" onclick="return confirm('Yakin ingin mencetak dokumen ini?')" target="_blank"><button class="btn btn-default"><i class="fas fa-print"></i> Print Data</button></a>
				<a href="<?php echo base_url('index.php/data/del').'?usr='.$this->input->get("usr", TRUE).'&dat='.$this->input->get("dat", TRUE); ?>" onclick="return confirm('Hapus data laporan ini?')"><button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Hapus Data</button></a>
				<?php }} else{echo "<h4 align='center'><i>In God We Trust</i></h4>";} ?>
			  </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('/style/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/passho.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/bootstrap.min.js');?>"></script>
</body>
</html>