			    <h3 class="panel-title"><i class="fas fa-file-alt"></i> Buat Data Laporan</h3>
			  </div>
			  <!--Bawah ini Body Gan bukan Bodyguard-->
			  <div class="panel-body">
			  	<?php if(isset($error)) { echo $error; }; if($this->dataio->chk('tbl_data', array('year'=>date("Y"), 'username'=>$this->session->userdata("uname")))){echo "<div class='row'><div class='col-md-2'><p style='font-size: 14em; font-weight: bold; color: #555;'>:)</p></div><div class='col-md-10'><h4 style='margin-top: 90px; color: #4CAF50;'>Tahun ini Anda telah mengirim Data!</h4><p>Terima kasih telah berpartisipasi mengisi data menggunakan ArsipIN, Silahkan diisi pada tahun berikutnya. Atau edit laporan Anda <a href='".base_url('index.php/edit')."' style='color: #008CBA;'>disini</a> sebelum laporan telah dibaca/dicetak oleh Pusat.</p></div></div>";}else{ ?>
			    <form method="POST" action="<?php echo base_url('index.php/data/action') ?>">
			    	<div class="form-group">
                    </div>
			    	<h4>ARSIP AKTIF</h4>
			    	<b>Pengurusan Surat Keluar</b>
			    	<div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7">Permohonan Surat Memakai Kode Verifikasi</div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a1" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a1" value="0" required>TIDAK
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Memakai Buku Agenda</div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="b1" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="b1" value="0" required>TIDAK
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Penyimpanan Arsip</div>
                    		<div class="col-md-5">
                    			<div class="row">
    								<div class="col-md-6">
                    					<i>&nbsp;a. Folder</i>
                    				</div>
                    				<div class="col-md-6">
		                    			<label class="radio-inline">
		      								<input type="radio" name="c1" value="1" required>YA
		    							</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="c1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
		    							<i>&nbsp;b. Binder</i>
		    						</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="d1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="d1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
    									<i>&nbsp;c. Box Arsip</i>
    								</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="e1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="e1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;d. Filling Cabinet</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="f1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="f1" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Penataan Arsip</div>
                    		<div class="col-md-5">
                    			<div class="radio">
                    				<label><input type="radio" name="g1" value="1" required>Berdasarkan Klasifikasi</label>
    							</div>
    							<div class="radio">
    								<label><input type="radio" name="g1" value="2" required>Berdasarkan Urutan Waktu</label>
    							</div>
    							<div class="radio">
    								<label><input type="radio" name="g1" value="0" required>Ditumpuk Tidak Beraturan</label>
    							</div>
    						</div>
                    	</div>
                	</div>
                	<b>Pengurusan Surat Masuk</b>
                	<div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7">Memakai Buku Agenda</div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="h1" value="1" required>SUDAH
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="h1" value="0" required>BELUM
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Memakai Lembar Disposisi</div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="i1" value="1" required>SUDAH
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="i1" value="0" required>BELUM
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Penyimpanan Arsip</div>
                    		<div class="col-md-5">
                    			<div class="row">
    								<div class="col-md-6">
                    					<i>&nbsp;a. Folder</i>
                    				</div>
                    				<div class="col-md-6">
		                    			<label class="radio-inline">
		      								<input type="radio" name="j1" value="1" required>YA
		    							</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="j1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
		    							<i>&nbsp;b. Binder</i>
		    						</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="k1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="k1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
    									<i>&nbsp;c. Box Arsip</i>
    								</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="l1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="l1" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;d. Filling Cabinet</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="m1" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="m1" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7">Penyimpanan Arsip</div>
                    		<div class="col-md-5">
                    			<div class="radio">
                    				<label><input type="radio" name="n1" value="1" required>Berdasarkan Klasifikasi</label>
    							</div>
    							<div class="radio">
    								<label><input type="radio" name="n1" value="2" required>Berdasarkan Urutan Waktu</label>
    							</div>
    							<div class="radio">
    								<label><input type="radio" name="n1" value="0" required>Ditumpuk Tidak Beraturan</label>
    							</div>
    						</div>
                    	</div>
                	</div>
                	<div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>SK Pengelola Kearsipan</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="o1" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="o1" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    </div>

                    <br>
                    <h4>ARSIP IN-AKTIF</h4>
			    	<div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip Tahun Sebelumnya</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a2" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a2" value="0" required>TIDAK
    							</label>
    							<div class="row" id="textboxes" style="display: none">
                    				<div class="col-md-2">Tahun</div>
                    				<div class="col-md-1">
    									<input type="text" name="b2" onkeypress="return hanyaAngka(event)" maxlength="4" />
    								</div>
                    			</div>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Penyimpanan Arsip</b></div>
                   			<div class="col-md-5">
	                   			<div class="row">
   									<div class="col-md-6">
                    					<i>&nbsp;a. Folder</i>
                    				</div>
                    				<div class="col-md-6">
		                    			<label class="radio-inline">
		      								<input type="radio" name="c2" value="1" required>YA
		    							</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="c2" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
		    							<i>&nbsp;b. Binder</i>
		    						</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="d2" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="d2" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
    									<i>&nbsp;c. Box Arsip</i>
    								</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="e2" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="e2" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;d. Filling Cabinet</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="f2" value="1" required>YA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="f2" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    						</div>
                    	</div>
                    </div>
                    <br>
                    <h4>ARSIP STATIS/DOKUMEN ARSIP(DESA)</h4>
                    <div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip Tanah/Leter C Desa</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip APBDes</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="b4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="b4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip Keuangan</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="c4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="c4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip Kependudukan</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="d4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="d4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Foto Mantan Kuwu</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="e4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="e4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Sejarah Desa</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="f4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="f4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip PILWU</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="g4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="g4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Peta Desa</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="h4" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="h4" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Arsip Kepegawaian</b></div>
                   			<div class="col-md-5">
	                   			<div class="row">
   									<div class="col-md-6">
                    					<i>&nbsp;a. SK Pamong</i>
                    				</div>
                    				<div class="col-md-6">
		                    			<label class="radio-inline">
		      								<input type="radio" name="i4" value="1" required>ADA
		    							</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="i4" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
		    							<i>&nbsp;b. Ijasah</i>
		    						</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="j4" value="1" required>ADA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="j4" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
    									<i>&nbsp;c. Akte Kelahiran</i>
    								</div>
    								<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="k4" value="1" required>ADA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="k4" value="0" required>TIDAK
	    								</label>
    								</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;d. Kartu Keluarga</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="l4" value="1" required>ADA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="l4" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;e. Buku Nikah</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="m4" value="1" required>ADA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="m4" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    							<div class="row">
    								<div class="col-md-6">
	    								<i>&nbsp;d. KTP</i>
	    							</div>
	    							<div class="col-md-6">
	                    				<label class="radio-inline">
	      									<input type="radio" name="n4" value="1" required>ADA
	    								</label>
	    								<label class="radio-inline">
	      									<input type="radio" name="n4" value="0" required>TIDAK
	    								</label>
	    							</div>
    							</div>
    						</div>
                    	</div>
                    </div>
                    <br>
                    <br>
                    <h4>ADAT DESA</h4>
                    <div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>Mapag Sri</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a5" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a5" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Sedekah Buni</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="b5" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="b5" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Ngunjung/Baritan</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="c5" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="c5" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Kesenian Daerah</b></div>
                    		<div class="col-md-5">
      							<textarea class="form-control" name="d5" rows="2"></textarea>
    						</div>
                    	</div>
                    </div>
                    <br>
                    <h4>SARANA KEARSIPAN</h4>
                    <div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>Box Arsip</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a6" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a6" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Folder/Map</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="b6" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="b6" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Skat</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="c6" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="c6" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Label</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="d6" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="d6" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Filling Cabinet</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="e6" value="1" required>ADA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="e6" value="0" required>TIDAK ADA
    							</label>
    						</div>
                    	</div>
                    </div>
                    <br>
                    <h4>KESIMPULAN</h4>
                    <div class="form-group">
                    	<div class="row">
                    		<div class="col-md-7"><b>SDM Kurang Memadai</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="a7" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="a7" value="0" required>TIDAK
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Belum Memahami Aturan Tata Kearsipan</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="b7" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="b7" value="0" required>TIDAK
    							</label>
    						</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-7"><b>Keterbatasan Sarana & Prasarana</b></div>
                    		<div class="col-md-5">
                    			<label class="radio-inline">
      								<input type="radio" name="c7" value="1" required>YA
    							</label>
    							<label class="radio-inline">
      								<input type="radio" name="c7" value="0" required>TIDAK
    							</label>
    						</div>
                    	</div>
                    </div>
                    <br>
                    <h4>Hasil Temuan</h4>
                    <textarea class="form-control" name="a3" rows="4"></textarea>
                    <br>
                    <button class="btn btn-success" name="btn-add" type="submit"><i class="fas fa-paper-plane"></i> Kirim Data</button>
                </form>
            <?php } ?>
			  </div>
			  <!--Atas ane Div Body The Body-->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('/style/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/passho.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/bootstrap.min.js');?>"></script>
<script type="text/javascript">
	$(function() {
	    $('input[name="a2"]').on('click', function() {
	        if ($(this).val() == '1') {
	            $('#textboxes').show();
	        }
	        else {
	            $('#textboxes').hide();
	        }
	    });
	});
</script>
</body>
</html>