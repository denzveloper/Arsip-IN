<h3 class="panel-title"><i class="fas fa-chart-pie"></i> Lihat Chart</h3>
</div>
<div class="panel-body">
<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url();?>style/js/jquery-2.0.3.js"></script>
<script src="<?php echo base_url();?>style/js/hich/highcharts.js"></script>
<!-- end load library -->

<?php
    /* Mengambil query report*/
    //report tahun ini yang udah kirim data
    $a=0;
    $b=0;
    if($fin != FALSE){foreach($fin as $fin){
      $a++;
    }}else{
      $a=0;
    }

    if($user != FALSE){foreach($user as $user) {
        $b++;
    }}else{
      $b=0;
    }
    $x = array("Sudah", "Belum");
    $b = array($a, $b-$a);
    //report 10 t'akhir tahun
    $i=date("Y");
    $hit=$i-10;
    while($hit <= $i) {
        $o=0;
        $kuy=$this->dataio->viewmin(array('year'=>$hit));
        foreach ($kuy as $kuy) {
            $o++;
        }
        $y[]=$hit;
        $z[]=$o;

        $hit++;
    }
?>

<!-- Load chart dengan menggunakan ID -->
<div id="report"></div>
<hr>
<div id="year"></div>
<!-- END load chart -->

<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#report').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Laporan Yang Belum dan Yang Sudah',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Laporan ArsipIN yang selesai dan yang belum dikirim per tahun ini',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($x);?>
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'Jumlah yang <b>' + this.x + '</b> adalah <b>' + Highcharts.numberFormat(this.y,0) + '</b>, dari laporan tahun ini.';
             }
          },
        series: [{
            name: 'Jumlah Laporan',
            data: <?php echo json_encode($b);?>,
            shadow : false,
            color: '#2d89ef',
            dataLabels: {
                enabled: true,
                color: '#1e7145',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>


<script type="text/javascript">
$(function () {
    $('#year').highcharts({
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Laporan 10 Tahun Terakhir',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Laporan ArsipIN yang ada selama satu dekade terakhir',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($y);?>
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'Jumlah pada tahun <b>' + this.x + '</b> adalah <b>' + Highcharts.numberFormat(this.y,0) + '</b> laporan.';
             }
          },
        series: [{
            name: 'Jumlah Laporan',
            data: <?php echo json_encode($z);?>,
            shadow : false,
            color: '#33cc33',
            dataLabels: {
                enabled: true,
                color: 'tomato',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
</div>
<script type="text/javascript" src="<?php echo base_url('/style/js/passho.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/bootstrap-select.js');?>"></script>
</body>
</html>
