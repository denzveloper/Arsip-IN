			    <h3 class="panel-title"><i class="fas fa-file-alt"></i> Daftar Laporan Oleh <b><?php if($this->dataio->chk_us(array('username' => $this->input->get("usr", TRUE)))){ echo $this->dataio->getwho($this->input->get("usr", TRUE))->nama_user;}else{ echo $this->input->get("usr", TRUE); } ?></b></h3>
			  </div>
			  <div class="panel-body">
			  	<?php if($this->dataio->chk_us(array('username' => $this->input->get("usr", TRUE)))){ ?>
			    <h4>Daftar Data Berdasarkan Tahun</h4>
			    <table id="table_id" class="table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr><th>No.</th><th>Data Tahun</th></tr>
                    </thead>
                    <tbody>
			    	<?php $i=0; $list=$this->dataio->viewmin(array('username' => $this->input->get("usr", TRUE))); if($list != FALSE){foreach($list as $lst){ $i++; ?>
			    		<tr>
                            <td width="1"><?php echo $i;?></td>
			    			<td><i><b><a href="<?php echo base_url('index.php/data/show').'?usr='.$this->safe->convert($this->input->get("usr", TRUE),$this->session->userdata('namaus')).'&dat='.$lst->date ?>"><?php echo $lst->year; ?></a></b></i></td>
			    		</tr>
			    	<?php } }else{echo "<td colspan='2' align='center'><b style='color: #aaa;'><i>*NO DATA TO SHOW*</i></b></td>";} ?>
			    	</tbody>
                </table>
                <hr>
			    <h4>5 Daftar Data Terbaru</h4>
		    	<ol type="1">
			    	<?php $list=$this->dataio->viewmin(array('username' => $this->input->get("usr", TRUE))); if($list != FALSE){foreach($list as $lst){ ?>
				    	<li><b><i><a href="<?php echo base_url('index.php/data/show').'?usr='.$this->safe->convert($this->input->get("usr", TRUE),$this->session->userdata('namaus')).'&dat='.$lst->date ?>"><?php echo $lst->year; ?></a></i></b></li>
			    	<?php } }else{ echo "<i style='color: #aaa;'>Data belum pernah dibuat</i>";} ?>
			    </ol>
			<?php }else{ echo "<h4 align='center'><i>E Pluribus Unum</i></h4>";}?>
			  </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('/style/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('/style/datatables/DataTables-1.10.16/js/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable({ "aLengthMenu": [[5, 7, 10, 20, -1], [5, 7, 10, 20, "Semua"]],
        "iDisplayLength": 5, "language": {"url": "<?php echo base_url('/style/datatables/DataTables-1.10.16/js/Indonesian.json');?>"}
        });
  } );
</script>
</body>
</html>