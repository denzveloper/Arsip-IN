<?php
if($dok != FALSE){foreach($dok as $dok){
  $loc = $this->dataio->getwho($dok->username)->place;
  $jab = $this->dataio->getwho($dok->username)->jabatan;
  $phn = $this->dataio->getwho($dok->username)->phone;
  $thn = date("Y", strtotime($dok->date));
  $nam = $this->dataio->getwho($dok->username)->nama_user;
  if($dok->arsip_thn_sblm==1){$yrs=$dok->tahun;}else{$yrs="-";}
}}else{
  echo "<br><h1 align='center'>-Was Gone!</h1><br><h4 align='center' style='color: white;'><i>Пролетарии всех стран, соединяйтесь!</i></h4>";
  exit;
}

//load model Convert
$this->load->library('Safe');

//Load Model PDF
$this->load->library('Pdf');
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle("Laporan $loc ($thn) Oleh $nam");
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(20);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->setPrintHeader(false);
$pdf->SetAuthor("$nam");
$pdf->SetDisplayMode('real', 'default');

$pdf->AddPage();

$html='<p style="text-align: center"><b>LAPORAN HASIL KEGIATAN PEMBINAAN DAN PENATAAN DOKUMEN/ARSIP DAERAH DI LINGKUNGAN PEMERINTAH KABUPATEN INDRAMAYU</b></p>';

  $html.='<table border=0>
            <tr>
              <td>Dasar Pelaksanaan</td><td>: Surat Tugas Kepala Dinas Kearsipan dan Perpuastakaan Kabupaten Indramayu, Nomor: 090/&nbsp;&nbsp;&nbsp;&nbsp;/Bid. Kearsipan Tanggal 2018. Perihal: Supervisi dan Pembinaan Kearsipan Daerah</td>
            </tr>
            <tr>
              <td>1. Nama</td><td>: '.$loc.'</td>
            </tr>
            <tr>
              <td>2. Nama Pengolah</td><td>: '.$nam.'</td>
            </tr>
            <tr>
              <td>3. Jabatan</td><td>: '.$jab.'</td>
            </tr>
            <tr>
              <td>4. No Telepon</td><td>: '.$phn.'</td>
            </tr>
            <tr>
              <td>5. Hari/Tanggal</td><td>: '.strftime("%A, %d %B %Y", strtotime($dok->date)).'</td>
            </tr>
            <tr>
              <td>6. Waktu</td><td>: '.date("H:i", strtotime($dok->date)).'</td>
            </tr>
            </table>
            ';

    $html.='<p><b>KONDISI SAAT DIKUNJUNGI</b><br>
            <b>A. ARSIP AKTIF</b><br>&nbsp;&nbsp;
            1. Pengurusan Surat Keluar<br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>a. Penomoran menggunakan kode klarifikasi</td><td>: '.$this->safe->auto($dok->k_no_srt_pk_kd_kls).'</td>
                </tr>
                <tr>
                  <td>b. Memakai buku agenda</td><td>: '.$this->safe->auto($dok->k_pk_nk_ag).'</td>
                </tr>
                <tr>
                  <td colspan=2>c. Penyimpanan Arsip</td>
                </tr>
                <tr>
                  <td>&nbsp;1. Folder</td><td>: '.$this->safe->auto($dok->k_sim_ar_fol).'</td>
                </tr>
                <tr>
                  <td>&nbsp;2. Binder</td><td>: '.$this->safe->auto($dok->k_sim_ar_bin).'</td>
                </tr>
                <tr>
                  <td>&nbsp;3. Box Arsip</td><td>: '.$this->safe->auto($dok->k_sim_ar_box).'</td>
                </tr>
                <tr>
                  <td>&nbsp;4. Filling Cabinet</td><td>: '.$this->safe->auto($dok->k_sim_ar_cab).'</td>
                </tr>
                <tr>
                  <td>d. Penataan Arsip</td><td>: '.$this->safe->autox($dok->k_tata_arsip).'</td>
                </tr>
              </table><br><br>&nbsp;&nbsp;
            2. Pengurusan Surat Masuk<br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>a. Memakai buku agenda</td><td>: '.$this->safe->auto($dok->m_pk_agenda).'</td>
                </tr>
                <tr>
                  <td>b. Memakai lembar disposisi</td><td>: '.$this->safe->auto($dok->m_pk_lmbr_dispo).'</td>
                </tr>
                <tr>
                  <td colspan=2>c. Penyimpanan Arsip</td>
                </tr>
                <tr>
                  <td>&nbsp;1. Folder</td><td>: '.$this->safe->auto($dok->m_sim_ar_fol).'</td>
                </tr>
                <tr>
                  <td>&nbsp;2. Binder</td><td>: '.$this->safe->auto($dok->m_sim_ar_bin).'</td>
                </tr>
                <tr>
                  <td>&nbsp;3. Box Arsip</td><td>: '.$this->safe->auto($dok->m_sim_ar_box).'</td>
                </tr>
                <tr>
                  <td>&nbsp;4. Filling Cabinet</td><td>: '.$this->safe->auto($dok->m_sim_ar_cab).'</td>
                </tr>
                <tr>
                  <td>d. Penataan Arsip</td><td>: '.$this->safe->autox($dok->m_tata_arsip).'</td>
                </tr>
              </table>
              <br><br>
              <table>
                <tr>
                  <td>&nbsp;3. SK Pengelola Kearsipan</td><td>&nbsp;: '.$this->safe->auto($dok->sk_olah_arsip).'</td>
                </tr>
              </table>
              <br><br>
              <b>B. ARSIP IN-AKTIF</b><br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>1. Arsip Tahun Sebelumnya</td><td>: '.$this->safe->autoa($dok->arsip_thn_sblm).'</td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;Tahun </td><td>: '.$yrs.'</td>
                </tr>
                <tr>
                  <td colspan=2>2. Penyimpanan Arsip</td>
                </tr>
                <tr>
                  <td>&nbsp;1. Folder</td><td>: '.$this->safe->auto($dok->sim_ar_fol).'</td>
                </tr>
                <tr>
                  <td>&nbsp;2. Binder</td><td>: '.$this->safe->auto($dok->sim_ar_box).'</td>
                </tr>
                <tr>
                  <td>&nbsp;3. Box Arsip</td><td>: '.$this->safe->auto($dok->sim_ar_gdng).'</td>
                </tr>
                <tr>
                  <td>&nbsp;4. Filling Cabinet</td><td>: '.$this->safe->auto($dok->sim_ar_lemari).'</td>
                </tr>
              </table>
            <br pagebreak="true" />
            <b>C. ARSIP STATIS</b><br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>1. Arsip Tanah/Leter C Desa</td><td>: '.$this->safe->autoa($dok->ar_tanah).'</td>
                </tr>
                <tr>
                  <td>2. Arsip APBDes</td><td>: '.$this->safe->autoa($dok->ar_apbdes).'</td>
                </tr>
                <tr>
                  <td>3. Arsip Keuangan</td><td>: '.$this->safe->autoa($dok->ar_kuang).'</td>
                </tr>
                <tr>
                  <td>4. Arsip Kependudukan</td><td>: '.$this->safe->autoa($dok->ar_kpdn).'</td>
                </tr>
                <tr>
                  <td>5. Foto Mantan Kuwu/Kepala Desa</td><td>: '.$this->safe->autoa($dok->fto_mntn_kuwu).'</td>
                </tr>
                <tr>
                  <td>6. Sejarah Desa Desa</td><td>: '.$this->safe->autoa($dok->sjrh_desa).'</td>
                </tr>
                <tr>
                  <td>7. Arsip PILWU</td><td>: '.$this->safe->autoa($dok->ar_pilwu).'</td>
                </tr>
                <tr>
                  <td>8. Peta Desa</td><td>: '.$this->safe->autoa($dok->pta_desa).'</td>
                </tr>
                <tr>
                  <td colspan=2>9. Arsip Kepegawaian</td>
                </tr>
                <tr>
                  <td>&nbsp;a). SK Pamong</td><td>: '.$this->safe->autoa($dok->ar_kep_skpmng).'</td>
                </tr>
                <tr>
                  <td>&nbsp;b). Ijasah</td><td>: '.$this->safe->autoa($dok->ar_kep_ijazah).'</td>
                </tr>
                <tr>
                  <td>&nbsp;c). Akte Kelahiran</td><td>: '.$this->safe->autoa($dok->ar_kep_akta).'</td>
                </tr>
                <tr>
                  <td>&nbsp;d). Kartu Keluarga</td><td>: '.$this->safe->autoa($dok->ar_kep_kk).'</td>
                </tr>
                <tr>
                  <td>&nbsp;e). Buku Nikah</td><td>: '.$this->safe->autoa($dok->ar_kep_bknkh).'</td>
                </tr>
                <tr>
                  <td>&nbsp;f). Kartu Tanda Penduduk</td><td>: '.$this->safe->autoa($dok->ar_kep_ktp).'</td>
                </tr>
              </table><br><br>
              <b>D. ADAT DESA/ADAT ISTIADAT</b><br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>1. Mapag Sri</td><td>: '.$this->safe->autoa($dok->mpg_sri).'</td>
                </tr>
                <tr>
                  <td>2. Sedekah Bumi</td><td>: '.$this->safe->autoa($dok->sdkh_bumi).'</td>
                </tr>
                <tr>
                  <td>3. Ngunjung/Baritan</td><td>: '.$this->safe->autoa($dok->ngnjng_bar).'</td>
                </tr>
                <tr>
                  <td>4. Kesenian Daerah/Tradisional</td><td>: '.$dok->seni_daerah.'</td>
                </tr>
                </table><br><br>
                <b>E. SARANA KEARSIPAN</b><br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>1. Box Arsip</td><td>: '.$this->safe->autoa($dok->s_box_ar).'</td>
                </tr>
                <tr>
                  <td>2. Folder/Map</td><td>: '.$this->safe->autoa($dok->s_fol_map).'</td>
                </tr>
                <tr>
                  <td>3. Skat</td><td>: '.$this->safe->autoa($dok->s_skat).'</td>
                </tr>
                <tr>
                  <td>4. Label</td><td>: '.$this->safe->autoa($dok->s_label).'</td>
                </tr>
                <tr>
                  <td>5. Filling Cabinet</td><td>: '.$this->safe->autoa($dok->s_fill_cab).'</td>
                </tr>
                </table><br><br>
                <b>F. KESIMPULAN</b><br>&nbsp;&nbsp;&nbsp;
              <table>
                <tr>
                  <td>1. SDM Kurang Memadai</td><td>: '.$this->safe->auto($dok->sdm_krg_mmdai).'</td>
                </tr>
                <tr>
                  <td>2. Belum Memahami Aturan Pengisian Arsip</td><td>: '.$this->safe->auto($dok->blm_phm_atrn).'</td>
                </tr>
                <tr>
                  <td>3. Keterbatasan Sarana Dan Prasarana</td><td>: '.$this->safe->auto($dok->bts_sarana).'</td>
                </tr>
                </table><br><br>
                <b>G. HASIL TEMUAN</b><br>&nbsp;&nbsp;&nbsp;
                <table>
                  <tr>
                    <td>'.$dok->hsl_temu.'</td>
                  </tr>
                </table>
              </p>';

//$html.='';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output("ArsipIN-Laporan $loc ($thn) Oleh $nam.pDf", 'I');

//echo $this->safe->auto($dok->k_no_srt_pk_kd_kls);
?>